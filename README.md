# vib2_tibbo_firmware

This repo contains the firmware for the Raven Vehicle Interface Board (VIB) rev 2.0.

The VIB 2.0 uses a [Tibbo](https://tibbo.com/) [EM2000](https://tibbo.com/store/modules/em2000.html) programmable embedded module.   The firmware is built programmed to the EM2000 in Tibbo's [TIDE](https://tibbo.com/programmable.html#tide) Windows-based IDE.

Documentation on the user-facing interfaces offered by the VIB, and associated user-space tools is in the [Raven Wiki](https://gitlab.com/apl-ocean-engineering/raven/raven_wiki/-/wikis/Hardware/Vehicle-Interface-Board-2.0).

Related repositories:
 * Kicad design files for the VIB: [raven_pcb_designs](https://gitlab.com/apl-ocean-engineering/raven/raven_pcb_designs)
 * Userspace tools: [vib2_driver](https://gitlab.com/apl-ocean-engineering/raven/vib2_driver).


 # Building/installing the firmware

Rebuilding the firmware requires the [TIDE](https://tibbo.com/programmable.html#tide) Windows-based IDE.  Open this repo in the IDE.

TIDE reprograms the module over ethernet; ensure the module is on the same network as the Windows PC.

In the dropdown at the top, select a "Release" build, then "Build and Upload."   This will compile the code, and if successful, upload it to the module.

Note that on restart the module will turn *off* all A10 and VPSU circuits.   Plan accordingly.

# License

This code is released under the [BSD 3-Clause License.](LICENSE)
